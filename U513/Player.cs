﻿using System;

namespace U513
{
	internal class Player : Attendee
	{
		//	float => 32-bit
		// double => 64-bit
		public double Height { get; set; }
		public string Position { get; set; }
		public string Club { get; set; }
		public bool Invited { get; set; }
		public bool Captain { get; set; }

		public override void Parse(string[] data)
		{
			Height = Convert.ToDouble(data[4]);
			Position = data[5];
			Club = data[6];
			Invited = data[7] == "TRUE";
			Captain = data[8] == "TRUE";
			base.Parse(data);
		}
	}
}