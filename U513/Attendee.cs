﻿using System;

namespace U513
{
	internal class Attendee
	{
		public string Name { get; set; }
		public string Surname { get; set; }
		public DateTime BirthDate { get; set; }


		public virtual void Parse(string[] data)
		{
			Name = data[1];
			Surname = data[2];
			BirthDate = Convert.ToDateTime(data[3]);
		}

		//public override string ToString() => 
		
	}
}