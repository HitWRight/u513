﻿using System;
using System.Collections.Generic;
using System.IO;

namespace U513
{
	internal class Camp
	{
		public int Year { get; set; }
		public DateTime BegginingDateTime { get; set; }
		public DateTime EndingDateTime { get; set; }

		public IList<Attendee> Attendees = new List<Attendee>();

		public static Camp ParseFile(FileInfo file)
		{
			Camp r = new Camp();
			var rd = new StreamReader(file.Open(FileMode.Open));
			r.Year = Convert.ToInt32(rd.ReadLine());
			r.BegginingDateTime = Convert.ToDateTime(rd.ReadLine());
			r.EndingDateTime = Convert.ToDateTime(rd.ReadLine());

			string line;
			while((line = rd.ReadLine()) != null)
			{
				Attendee a;
				if (line.Substring(0, 1).ToUpper() == "K")
					a = new Player();
				else
					a = new Support();

				a.Parse(line.Split(";"));
				r.Attendees.Add(a);
			}
			return r;
		}
	}
}