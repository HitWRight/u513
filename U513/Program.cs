﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace U513
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			IList<Camp> camps = new List<Camp>();
			foreach (var fil in Directory.GetFiles("Data"))
				camps.Add(Camp.ParseFile(new FileInfo(fil)));


			var players = camps.SelectMany(c => c.Attendees)
				.Where(a => a is Player p && p.Club == "Žalgiris");

			foreach (Player player in players.OrderBy(a => a.Surname + a.Name))
				Console.WriteLine($"{player.Name} {player.Surname} {player.Position}");

			var file = "output\\senior.csv";
			foreach (Player p in camps.SelectMany(c => c.Attendees).Where(a => a is Player && (DateTime.Now - a.BirthDate).TotalDays / 365.25 > 30))
			{
				var wr = new StreamWriter(file);
				wr.WriteLine($"{p.Position} {p.Surname} {p.Name}");
			}
			foreach (Support s in camps.SelectMany(c => c.Attendees).Where(a => a is Support && (DateTime.Now - a.BirthDate).TotalDays / 365.25 > 50))
			{
				var wr = new StreamWriter(file, true);
				wr.WriteLine($"{s.WhatTheFuckIDo} {s.Surname} {s.Name}");
			}
		}
	}
}